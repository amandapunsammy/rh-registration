$(function () {
  console.log("test");
  let signUpForm = document.getElementById('registrationForm');
  let form = new FormData(signUpForm);
  form.append("email", "llakhyan@gmail.com");
  form.append("firstName", "Lucky");
  form.append("lastName", "Lakhyan");
  form.append("companyName", "Lakhyan Home");
  form.append("countryRegion", " Canada West");
  form.append("username", "lucky.lakhyan@shiftanalytics.ca");
  form.append("acceptTerms", "true");

  let settings = {
    url: "https://shiftanalyticsrhdemo.azurewebsites.net/license/home/SignUpApi",
    method: "POST",
    timeout: 0,
    processData: false,
    mimeType: "multipart/form-data",
    contentType: false,
    data: form,
  };

  $.ajax(settings).done(function (response) {
    console.log("RESPONSE============================================");
    console.log(response);
  });
});

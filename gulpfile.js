var gulp = require('gulp');
var gulpless = require('gulp-less');
var gulpautoprefixer = require('gulp-autoprefixer');

gulp.task('styles', function() {
  var srcIndexfile = './css/index.less';
  var temp = './compiled';
    return gulp
      .src(srcIndexfile)
      .pipe(gulpless())
      .pipe(gulpautoprefixer({browsers: ['last 2 versions', '>5%']}))
      .pipe(gulp.dest(temp));
})
